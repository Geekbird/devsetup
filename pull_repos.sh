#!/bin/bash
USER=$1
PASSWORD=$2

array=( repos devsetup test_ref software prod_ref play infra visgenie)
for i in "${array[@]}"
do
	echo "Pulling $i"
	git clone https://$USER:$PASSWORD@gitlab.com/Geekbird/$i.git
done
